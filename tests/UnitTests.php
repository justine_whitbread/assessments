<?php declare(strict_types=1);


namespace App;

use PHPUnit\Framework\TestCase;

require_once("./src/App/PublicHoliday.php");

class UnitTests extends TestCase
{
    protected static $holiday;

    public static function setUpBeforeClass(): void
    {
        self::$holiday[] = new PublicHoliday("rest",["date"=>["day"=>1,"month"=>1,"year"=>2021,"dayOfWeek"=>5]
            ,"name"=>[["lang"=>"en","text"=>"New Year's Day"]]],"ZAF");
        self::$holiday[] = new PublicHoliday("rest",["date"=>["day"=>27,"month"=>12,"year"=>2021,"dayOfWeek"=>1]
            ,"name"=>[["lang"=>"en","text"=>"Day of Goodwill"]],
            "flags"=>["ADDITIONAL_HOLIDAY"]],"ZAF");
    }

    /*
     * Test public holiday methods
     */
    public function testInstantiatePublicHoliday()
    {
        $this->assertSame(self::$holiday[0]->getName(),"New Year's Day");
    }
    public function testGetDayOfWeekName()
    {
        $this->assertSame(self::$holiday[0]->getDayOfWeekName(),"Friday");
    }

    public function testNotAdditionalHoliday()
    {
        $this->assertSame(self::$holiday[0]->isAdditionalHoliday(),false);
    }

    public function testIsAdditionalHoliday()
    {
        $this->assertSame(self::$holiday[1]->isAdditionalHoliday(),true);
    }
}