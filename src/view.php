<?php

namespace App;

function my_autoloader($class)
{
    include $class . '.php';
}

spl_autoload_register('App\my_autoloader');

//initialise the country and year
$default_year = date("Y");
$default_country = "ZAF";
$year = (isset($_GET["year"]) ? $_GET["year"] : $default_year);
$country_code = (isset($_GET["country"]) ? $_GET["country"] : $default_country);


//validate input to using regular expressions
if (preg_match("/^\d{4}$/", $year) == false or preg_match("/^[a-zA-Z]{3}$/", $country_code) == false) {
    //set year and country to default
    $year = $default_year;
    $country_code = $default_country;
}

//get the public holidays from the public holiday service
$service = new Services();
$holidays = $service->getPublicHolidays($year, $country_code);
$country_name = $service->getCountryCode($country_code);

?>
<!doctype html>
<html lang="en">
<head>
    <title>MDS Technologies Assessment</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <script src="js/bootstrap.bundle.min.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        Select Year
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <?php
                        echo "<li><a class='dropdown-item' href='view.php?year=2017&country=", $country_code, "'>2017</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=2018&country=", $country_code, "'>2018</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=2019&country=", $country_code, "'>2019</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=2020&country=", $country_code, "'>2020</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=2021&country=", $country_code, "'>2021</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=2022&country=", $country_code, "'>2022</a></li>";
                        ?>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        Select Country
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <?php
                        echo "<li><a class='dropdown-item' href='view.php?year=", $year, "&country=ZAF'>South Africa</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=", $year, "&country=DEU'>Germany</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=", $year, "&country=CAN'>Canada</a></li>";
                        echo "<li><a class='dropdown-item' href='view.php?year=", $year, "&country=BEL'>Belgium</a></li>";
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Public Holidays, <?php echo $country_name, ", ", $year; ?></th>
        <?php
        echo "<td scope='col'><a href='download.php?year=", $year, "&country=", $country_code, "'>Download as PDF</td>"
        ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($holidays as $holiday) {
        echo "<tr>";
        echo "<td scope='row'>";
        if ($holiday->isAdditionalHoliday()) {
            echo "Public holiday ";
        }
        echo $holiday->getName();
        if ($holiday->isAdditionalHoliday()) {
            echo " observed";
        }
        echo "</td>";
        echo "<td scope='row'>", $holiday->getDayOfWeekName(), ", ", $holiday->getDateString(), "</td>";
        echo "</tr>";
    }

    ?>

    </tbody>
</table>
</body>
</html>

