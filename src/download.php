<?php

namespace App;
function my_autoloader($class)
{
    include $class . '.php';
}

spl_autoload_register('App\my_autoloader');

$default_year = date("Y");
$default_country = "ZAF";
$year = (isset($_GET["year"]) ? $_GET["year"] : $default_year);
$country_code = (isset($_GET["country"]) ? $_GET["country"] : $default_country);

//validate input to give human readable error message
if (preg_match("/^\d{4}$/", $year) == false or preg_match("/^[a-zA-Z]{3}$/", $country_code) == false) {
    ?>
    <html>
    <head>
        <title>MDS Technologies Assessment</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/bootstrap.bundle.js"></script>

    </head>
    <body>
    <h3>There was a problem downloading the data</h3>
    </body>
    </html>
    <?php
    exit(0);
}
$service = new Services();
$holidays = $service->getPublicHolidays($year, $country_code);

require('fpdf/fpdf.php');

$pdf = new \FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);
$pdf->Cell(0, 10, "Public Holidays, South Africa " . $year, 0, 1);
$pdf->SetFont('Arial', '', 12);

foreach ($holidays as $holiday) {
    $line1 = "";
    if ($holiday->isAdditionalHoliday()) {
        $line1 .= "Public holiday ";
    }
    $line1 .= $holiday->getName();
    if ($holiday->isAdditionalHoliday()) {
        $line1 .= " observed";
    }
    $line2 = $holiday->getDayOfWeekName() . ", " . $holiday->getDateString();

    $pdf->Cell(120, 10, $line1, 0);
    $pdf->Cell(0, 10, $line2, 0, 1);
}

$pdf->Output('D', 'public_holidays.pdf');