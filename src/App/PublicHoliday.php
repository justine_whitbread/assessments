<?php

namespace App;

/*
 * Model for public holiday entity
 */

class PublicHoliday
{
    const DAYSOFWEEK = array("1" => "Monday", "2" => "Tuesday", "3" => "Wednesday", "4" => "Thursday", "5" => "Friday",
        "6" => "Saturday", "7" => "Sunday");
    const MONTHOFYEAR = array("1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr", "5" => "May", "6" => "Jun", "7" => "Jul", "8" => "Aug"
    , "9" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");

    /**
     * @var integer
     */
    private $day;
    /**
     * @var integer
     */
    private $month;
    /**
     * @var integer
     */
    private $year;
    /**
     * @var integer
     */
    private $day_of_week;
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $additionalHoliday;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * PublicHoliday constructor.
     */
    public function __construct($input_source = null, $input_data = null, $country_code = null)
    {
        if ($input_data !== null) {
            switch ($input_source) {
                case "rest":
                    //populate object with public holiday data from rest service
                    $this->day = $input_data["date"]["day"];
                    $this->month = $input_data["date"]["month"];
                    $this->year = $input_data["date"]["year"];
                    $this->day_of_week = $input_data["date"]["dayOfWeek"];
                    $this->name = $input_data["name"][0]["text"];
                    $this->additionalHoliday = false;
                    $this->countryCode = $country_code;
                    if (isset($input_data["flags"]) and in_array("ADDITIONAL_HOLIDAY", $input_data["flags"])) {
                        $this->additionalHoliday = true;
                    }
                    break;
                case "db":
                    //populate object with public holiday data from database
                    $this->day = (int)$input_data["holiday_day"];
                    $this->month = (int)$input_data["holiday_month"];
                    $this->year = (int)$input_data["holiday_year"];
                    $this->day_of_week = (int)$input_data["day_of_week"];
                    $this->name = $input_data["holiday_name"];
                    $this->additionalHoliday = false;
                    $this->countryCode = $input_data["country_code"];
                    if (!empty($input_data["additional_day"])) {
                        $this->additionalHoliday = (bool)$input_data["additional_day"];
                    }
                    break;
            }

        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        //build id from holiday year,month,day
        return $this->countryCode . $this->year . $this->month . $this->day;
    }

    /**
     * @return string
     */
    public function getDateString(): string
    {
        //return date in human readable format
        return str_pad($this->day, STR_PAD_LEFT, 2) . " " . self::MONTHOFYEAR[$this->month] . " " . $this->year;
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->day;
    }

    /**
     * @param int $day
     */
    public function setDay(int $day)
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth(int $month)
    {
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getDayOfWeek(): int
    {
        return $this->day_of_week;
    }

    /**
     * @param int $day_of_week
     */
    public function setDayOfWeek(int $day_of_week)
    {
        $this->day_of_week = $day_of_week;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDayOfWeekName(): string
    {
        return self::DAYSOFWEEK[$this->day_of_week];
    }

    /**
     * @return bool
     */
    public function isAdditionalHoliday(): bool
    {
        return $this->additionalHoliday;
    }

    /**
     * @param bool $additionalHoliday
     */
    public function setAdditionalHoliday(bool $additionalHoliday)
    {
        $this->additionalHoliday = $additionalHoliday;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }


}