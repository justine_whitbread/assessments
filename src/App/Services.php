<?php

namespace App;

class Services
{


    const URL = "https://kayaposoft.com/enrico/json/v2.0/";
    const ENDPOINT = "action=getHolidaysForYear&holidayType=public_holiday";
    const COUNTRY = array("ZAF" => "South Africa", "DEU" => "Germany", "CAN" => "Canada", "BEL" => "Belgium");
    /**
     * sqllite pdo handle
     * @var \PDO
     */
    private $myPDO;

    /**
     * Services constructor.
     */
    public function __construct()
    {

        $db_dsn = 'sqlite:' . __DIR__ . '\..\..\database.sqlite';//don't do this at home
        //connect to sqlite database
        $this->myPDO = new \PDO($db_dsn) or die("failed to connect");

    }

    /**
     * Lookup country name using alpha-3 code
     *
     * @param $country_code
     * @return string
     */
    public function getCountryCode($country_code): string
    {

        return self::COUNTRY[strtoupper($country_code)];
    }

    /**
     * loop through public holidays array and insert data into public_holidays table
     * @param array<PublicHoliday> $public_holidays
     */
    public function populateDatabase(array $public_holidays)
    {
        $insert_cmd = "REPLACE INTO public_holidays(Id,holiday_name,holiday_day,holiday_month,holiday_year,additional_day,
        day_of_week,country_code) values (:id,:holiday_name,:holiday_day,:holiday_month,:holiday_year,:holiday_additional_day,:day_of_week,
:country_code)";
        $stmt = $this->myPDO->prepare($insert_cmd);

        if ($stmt == false) {
            //todo - better exception handling
            throw new \Exception("Something went wrong,could not prepare statement");
        }
        foreach ($public_holidays as $holiday) {
            //initialise temporary variables;
            $id = $holiday->getId();
            $name = $holiday->getName();
            $day = $holiday->getDay();
            $month = $holiday->getMonth();
            $year = $holiday->getYear();
            $add = $holiday->isAdditionalHoliday();
            $day_week = $holiday->getDayOfWeek();
            $country_code = $holiday->getCountryCode();

            //bind parameters
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':holiday_name', $name);
            $stmt->bindParam(':holiday_day', $day);
            $stmt->bindParam(':holiday_month', $month);
            $stmt->bindParam(':holiday_year', $year);
            $stmt->bindParam(':holiday_additional_day', $add);
            $stmt->bindParam(':day_of_week', $day_week);
            $stmt->bindParam(':country_code', $country_code);

            $result = $stmt->execute();
            if ($result == false) {
                //todo - better exception handling
                throw new \Exception("Something went wrong,could not prepare statement");
            }
        }
    }


    /**
     * check if public holidays exists in table for given year & country
     *
     * @param int $year
     * @param string $country
     * @return mixed
     * @throws \Exception
     */
    public function countDaysInDB(int $year, string $country)
    {
        $query_cmd = "select count(*) as count from public_holidays where holiday_year=:holiday_year and country_code=:country_code";
        $stmt = $this->myPDO->prepare($query_cmd);
        if ($stmt == false) {
            //todo - better exception handling
            throw new \Exception("Something went wrong,could not prepare statement");
        }
        $stmt->bindParam(':holiday_year', $year);
        $stmt->bindParam(':country_code', $country);
        $result = $stmt->execute();
        if ($result == false) {
            //todo - better exception handling
            throw new \Exception("Something went wrong,could not prepare statement");
        }

        $count = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $count["count"];

    }

    /**
     * query public_holidays table for a given country and year and push data into array of type PublicHoliday
     *
     * @param int $year
     * @param string $country
     * @return array
     * @throws \Exception
     */
    public function getHolidaysFromDB(int $year, string $country)
    {
        $return_array = [];
        $query_cmd = "select * from public_holidays where holiday_year=:holiday_year and country_code=:country_code  
                        order by holiday_month,holiday_day";
        $stmt = $this->myPDO->prepare($query_cmd);
        if ($stmt == false) {
            //todo - better exception handling
            throw new \Exception("Something went wrong,could not prepare statement");
        }
        $stmt->bindParam(':holiday_year', $year);
        $stmt->bindParam(':country_code', $country);
        $result = $stmt->execute();
        if ($result == false) {
            //todo - better exception handling
            throw new \Exception("Something went wrong,could not prepare statement");
        }

        while ($holiday = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $return_array[] = new PublicHoliday("db", $holiday);
        }
        return $return_array;
    }


    /**
     * return the public holidays for a given country & year
     *
     * @param int $year
     * @param string $country
     * @return array
     * @throws \Exception
     */
    public function getPublicHolidays(int $year, string $country): array
    {
        /*
         * Try and populate the data from database and
         *  fallback to rest service if no data found
         */
        if ($this->countDaysInDB($year, $country) > 0) {
            return $this->getHolidaysFromDB($year, $country);
        }

        /*
         * Call the service to get the data for given year and persist to database
         */
        $url = self::URL . '?' . self::ENDPOINT . "&year=" . $year . "&country=" . strtolower($country);
        $holidays = $this->consumeService($url);
        $return_array = [];
        /*
         * perform basic sanity check on data and populate array of objects
         */
        foreach ($holidays as $holiday) {
            if (isset($holiday["date"]) === false || isset($holiday["name"]) === false) {
                //todo - better exception handling
                //data is invalid
                throw new \Exception("Something went wrong, service return invalid data set");
            }
            $return_array[] = new PublicHoliday("rest", $holiday, $country);
        }

        /*
         * write the data to public_holiday table
         */
        $this->populateDatabase($return_array);
        return $return_array;
    }

    /*
     * consume enrico REST service using curl
     */
    /**
     * @param $url
     * @return array
     * @throws \Exception
     */
    private function consumeService($url): array
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $data = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            curl_close($ch);
            //todo - better exception handling
            //if an error occurs then throw error
            // else if transforms the results into an array  and returns the array to the calling class
            if ($curl_errno > 0) {
                throw new \Exception("Something went wrong, could not get public holidays from service");
            } else {
                $public_holidays = json_decode($data, true);
                if ($public_holidays === null or is_array($public_holidays) === false) {
                    //invalid response received - throw error
                    throw new \Exception("Something went wrong, could not get public holidays from service");
                }
                return $public_holidays;
            }
        } catch (\Exception $ex) {
            throw new \Exception("Something went wrong, could not get public holidays from service");
        }
    }


}